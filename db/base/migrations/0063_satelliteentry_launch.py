# Generated by Django 4.0.10 on 2024-04-05 10:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0062_launch'),
    ]

    operations = [
        migrations.AddField(
            model_name='satelliteentry',
            name='launch',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='embarked_in', to='base.launch'),
        ),
    ]
